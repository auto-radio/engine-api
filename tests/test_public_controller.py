#
# Aura Engine API (https://gitlab.servus.at/aura/engine-api)
#
# Copyright (C) 2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
test_public_controller.py.
"""

from __future__ import absolute_import

# from aura_engine_api.rest.models.track import Track  # noqa: E501
from . import BaseTestCase

# from flask import json
# from six import BytesIO


class TestPublicController(BaseTestCase):
    """
    PublicController integration test stubs.
    """

    def test_current_track(self):
        """Test case for current_track.

        Get current track
        """
        response = self.client.open("/api/v1/trackservice/current", method="GET")
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_list_tracks(self):
        """Test case for list_tracks.

        List recent tracks in the play-log
        """
        query_string = [
            ("fromDate", "2013-10-20T19:20:30+01:00"),
            ("toDate", "2013-10-20T19:20:30+01:00"),
            ("page", 56),
            ("limit", 50),
        ]
        response = self.client.open(
            "/api/v1/trackservice", method="GET", query_string=query_string
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    import unittest

    unittest.main()
