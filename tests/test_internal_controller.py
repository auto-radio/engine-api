#
# Aura Engine API (https://gitlab.servus.at/aura/engine-api)
#
# Copyright (C) 2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
test_internal_controller.py.
"""

from __future__ import absolute_import

import unittest

from flask import json

from aura_engine_api.rest.models.clock_info import ClockInfo  # noqa: E501

from . import BaseTestCase


class TestInternalController(BaseTestCase):
    """
    InternalController integration test stubs.
    """

    def test_add_playlog(self):
        """Test case for add_playlog.

        Adds an entry to the playlog
        """
        body = {"trackStart": "2018-08-12 16:41+01:00"}
        response = self.client.open(
            "/api/v1/playlog",
            method="POST",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))

    def test_add_playlog_emoji(self):
        """Test case for adding track titles containing emojis to playlog.

        Adds an entry to the playlog, fetches the new entry and compares their titles.
        """

        from_date = "2023-06-28 00:00+02:00"
        to_date = "2023-06-28 00:10+02:00"
        track_title = "Dynoro feat. Gigi DAgostino - In My Mind 🔊 BASS BOOSTED 🔥"

        body = {"trackStart": from_date, "trackTitle": track_title}
        response = self.client.open(
            "/api/v1/playlog",
            method="POST",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))

        query_string = [
            ("fromDate", from_date),
            ("toDate", to_date),
        ]
        response = self.client.open("/api/v1/playlog", method="GET", query_string=query_string)
        response_str = response.data.decode("utf-8")
        self.assert200(response, "Response body is : " + response_str)

        response_json = json.loads(response_str)
        fetched_title = response_json[0]["track_title"]

        self.assertEqual(fetched_title, track_title)

    def test_clock_info(self):
        """Test case for clock_info.

        Get all information to display the studio clock
        """
        response = self.client.open("/api/v1/clock", method="GET")
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_get_active_source(self):
        """Test case for get_active_source.

        Get active play-out source (engine1, engine2)
        """
        response = self.client.open("/api/v1/source/active", method="GET")
        # TEMP FIX: we need some data to succeed with assert200
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))

    def test_get_source_health(self):
        """Test case for get_source_health.

        Get most recent health info
        """
        response = self.client.open(
            "/api/v1/source/health/{number}".format(number=2), method="GET"
        )
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_list_playlog(self):
        """Test case for list_playlog.

        List tracks in the play-log since the given timestamp
        """
        query_string = [
            ("fromDate", "2013-10-20T19:20:30+01:00"),
            ("toDate", "2013-10-20T19:20:30+01:00"),
            ("page", 56),
            ("limit", 200),
            ("skipSynced", True),
        ]
        response = self.client.open("/api/v1/playlog", method="GET", query_string=query_string)
        self.assert200(response, "Response body is : " + response.data.decode("utf-8"))

    def test_log_source_health(self):
        """Test case for log_source_health.

        Log health info
        """
        body = {
            "details": "",
            "isHealthy": True,
            "logTime": "2014-11-21T17:16:23+01:00",
        }
        response = self.client.open(
            "api/v1/source/health/{number}".format(number=2),
            method="POST",
            data=json.dumps(body),
            content_type="application/json",
        )
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))

    def test_set_active_source(self):
        """Test case for set_active_source.

        Set active play-out source (engine1, engine2)
        """
        response = self.client.open(
            "/api/v1/source/active/{number}".format(number=2), method="PUT"
        )
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))

    def test_set_clock_info(self):
        """Test case for set_clock_info.

        Set current studio clock information such as timeslot info and track-list for engine 1 or 2
        within the Engine API database.
        """
        body = ClockInfo(engine_source=1)
        response = self.client.open(
            "/api/v1/clock", method="PUT", data=json.dumps(body), content_type="application/json"
        )
        self.assert204(response, "Response body is : " + response.data.decode("utf-8"))


if __name__ == "__main__":
    unittest.main()
