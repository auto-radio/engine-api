import logging

from flask_testing import TestCase

from aura_engine_api.app import app
from aura_engine_api.rest.encoder import JSONEncoder


class BaseTestCase(TestCase):
    def create_app(self):
        logging.getLogger("connexion.operation").setLevel("ERROR")
        app.json_encoder = JSONEncoder
        return app

    def assert204(self, response, message=None):
        """Based on assert200 in parent class"""
        self.assertStatus(response, 204, message)
