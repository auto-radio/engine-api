-include build/base.Makefile
-include build/docker.Makefile

help::
	@echo "$(APP_NAME) targets:"
	@echo "    init.app        - init application environment"
	@echo "    init.dev        - init development environment"
	@echo "    spell           - check spelling of text"
	@echo "    format          - apply automatic formatting"
	@echo "    test            - run test suite"
	@echo "    log             - tail log file"
	@echo "    run             - start app"
	@echo "    run.gunicorn    - start app with gunicorn"
	@echo "    release         - tags and pushes a release with current version"
	$(call docker_help)


# Settings

AURA_ENGINE_API_HOST ?= 127.0.0.1
AURA_ENGINE_API_PORT ?= 8008
AURA_ENGINE_API_CONFIG := ${CURDIR}/config/engine-api.docker.yaml
AURA_LOGS := ${CURDIR}/logs
AURA_UID := 2872
AURA_GID := 2872

DOCKER_RUN = @docker run \
		--name $(APP_NAME) \
		--network="host" \
		--mount type=tmpfs,destination=/tmp \
		--env-file docker.env \
		-v "$(AURA_ENGINE_API_CONFIG)":"/etc/aura/engine-api.yaml":ro \
		-v "$(AURA_LOGS)":"/srv/logs" \
		-u $(AURA_UID):$(AURA_GID) \
		$(DOCKER_ENTRY_POINT) \
		autoradio/$(APP_NAME)


API_SCHDEF := $(CURDIR)/schemas/openapi-engine.yaml
API_GENDIR := $(CURDIR)/swagger_codegen_out
API_SRCDIR := $(API_GENDIR)/swagger_server
API_TGTDIR := $(CURDIR)/src/aura_engine_api/rest

define merge_api_dir
	@for FILE in $(shell ls $(1)); do \
		if [ -f $(1)/$${FILE} ]; then \
			git merge-file $(2)/$${FILE} $(2)/$${FILE} $(1)/$${FILE}; \
		fi \
	done
endef


# Targets

init.app:: pyproject.toml
	poetry install
	-cp -n config/sample.engine-api.yaml config/engine-api.yaml
	-cp -n config/sample/gunicorn/sample.gunicorn.conf.py config/gunicorn.conf.py

init.dev:: pyproject.toml
	poetry install --with dev
	poetry run pre-commit autoupdate
	poetry run pre-commit install
	-cp -n config/sample.engine-api.yaml config/engine-api.yaml


api::
	make api.gen
	make api.merge
	make api.clean

api.gen::
	@docker run \
		-v "$(API_SCHDEF)":"/schdef":ro \
		-v "$(API_GENDIR)":"/outdir":rw \
		swaggerapi/swagger-codegen-cli-v3 generate \
		-l python-flask \
		-i schdef \
		-o outdir

api.merge::
	$(call merge_api_dir,$(API_SRCDIR),$(API_TGTDIR))
	$(call merge_api_dir,$(API_SRCDIR)/controllers,$(API_TGTDIR)/controllers)
	$(call merge_api_dir,$(API_SRCDIR)/models,$(API_TGTDIR)/models)
	$(call merge_api_dir,$(API_SRCDIR)/test,$(CURDIR)/tests)
	
api.clean::
	@rm -rf $(API_GENDIR)


lint::
	poetry run python3 -m flake8 .

spell::
	poetry run codespell $(wildcard *.md) docs src tests config contrib

format::
	poetry run python3 -m isort .
	poetry run black .

test::
	poetry run python3 -m unittest discover . --pattern "test_*.py"

coverage:
	poetry run coverage run -m unittest discover . --pattern "test_*.py" && poetry run coverage report -m && poetry run coverage xml
	

log::
	tail -f logs/$(APP_NAME).log

run::
	poetry run python3 -m aura_engine_api.app

run.gunicorn::
	poetry run gunicorn -c config/gunicorn.conf.py aura_engine_api.app:app \
	--bind $(AURA_ENGINE_API_HOST):$(AURA_ENGINE_API_PORT)

release::
	$(eval VERSION := $(shell python3 -c 'import tomli; print(tomli.load(open("pyproject.toml", "rb"))["tool"]["poetry"]["version"])'))
	git tag $(VERSION)
	git push origin $(VERSION)
	@echo "Release '$(VERSION)' tagged and pushed successfully."