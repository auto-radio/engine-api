#
# Aura Engine API (https://gitlab.servus.at/aura/engine-api)
#
# Copyright (C) 2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
models.py.
"""

import contextlib
import datetime
import json

from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema, fields, post_dump
from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.event import listen
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()


class DB:
    """Database object."""

    Model = Base
    _factory = None

    @classmethod
    def bind(cls, engine):
        """Set session factory."""
        cls._factory = sessionmaker(autoflush=True, autocommit=False, bind=engine)
        cls.Model.metadata.create_all(engine)
        cls.Model.metadata.bind = engine

    @classmethod
    @property
    @contextlib.contextmanager
    def session(cls):
        """Return database session."""
        session = scoped_session(cls._factory)
        yield session
        session.close()


db = SQLAlchemy()
ma = Marshmallow()


class PlayLog(DB.Model):
    """
    Table holding play-log entries.
    """

    __tablename__ = "playlog"

    # Primary Key
    track_start = Column(DateTime, primary_key=True)

    # Columns
    track_artist = Column(String(256))
    track_album = Column(String(256))
    track_title = Column(String(256))
    track_duration = Column(Integer)
    track_type = Column(Integer)
    track_num = Column(Integer)
    playlist_id = Column(Integer)
    timeslot_id = Column(Integer)
    show_id = Column(Integer)
    show_name = Column(String(256))
    log_source = Column(
        Integer
    )  # The play-out source which this log is coming from (e.g. engine1, engine2)
    is_synced = Column(Boolean)  # Only relevant for main nodes, in a multi-node setup

    def __init__(self, data):
        """
        Initialize a trackservice entry.
        """
        self.track_start = data.track_start
        self.track_artist = data.track_artist
        self.track_album = data.track_album
        self.track_title = data.track_title
        self.track_duration = data.track_duration
        self.track_type = data.track_type
        self.track_num = data.track_num
        self.playlist_id = data.playlist_id
        self.timeslot_id = data.timeslot_id
        self.show_id = data.show_id
        self.show_name = data.show_name
        self.log_source = data.log_source
        self.is_synced = False
        if not self.track_duration:
            self.track_duration = 0

    def save(self):
        """
        Save changes.
        """
        with DB.session as session:
            session.add(self)
            session.commit()

    @staticmethod
    def get(start_time):
        """
        Select the playlog identified by start time.
        """
        with DB.session as session:
            track = (
                session.query(PlayLog)
                .filter(PlayLog.track_start <= str(start_time))
                .order_by(PlayLog.track_start.desc())
                .first()
            )
            return track

    @staticmethod
    def select_recent():
        """
        Select the most recent played track. Equals to the current track if it's still playing.
        """
        now = datetime.datetime.now()
        with DB.session as session:
            track = (
                session.query(PlayLog)
                .order_by(PlayLog.track_start.desc())
                .filter(PlayLog.track_start <= str(now))
                .first()
            )
            return track

    @staticmethod
    def select_current():
        """
        Select the currently playing track.
        """
        now = datetime.datetime.now()

        track = PlayLog.select_recent()

        if track:
            # Station fallback may not provide a duration. Take this one, it's better than nothing.
            if track.track_duration == 0 or not track.track_duration:
                return track

            # Preferably only get playlogs which are known for still being on air
            if track.track_start + datetime.timedelta(0, track.track_duration) > now:
                return track

        return None

    @staticmethod
    def select_for_timeslot(timeslot_id):
        """
        Select all playlogs which appear to be in the current timeslot.

        If playlogs without a valid timeslot are queried (-1), then only
        the last 50 total playlogs are respected.
        """
        before12h = datetime.datetime.now() - datetime.timedelta(hours=12)
        playlogs = None
        with DB.session as session:
            # Invalid Timeslot ID
            if timeslot_id == -1:
                playlogs = []
                result = (
                    session.query(PlayLog)
                    .order_by(PlayLog.track_start.desc())
                    .filter(PlayLog.track_start >= str(before12h))
                    .limit(50)
                )
                for playlog in result.all():
                    if playlog.timeslot_id != -1:
                        break
                    playlogs.append(playlog)

            # Valid Timeslot ID
            else:
                result = (
                    session.query(PlayLog)
                    .order_by(PlayLog.track_start.desc())
                    .filter(PlayLog.timeslot_id == timeslot_id)
                )
                playlogs = result.all()

            return playlogs

    @staticmethod
    def paginate(page, page_size, from_time=None, to_time=None, skip_synced=False):
        """
        Return a list of entries for a given page and an start time (optional).
        """
        if not from_time:
            from_time = datetime.datetime.utcnow().date()
        if not to_time:
            to_time = datetime.datetime.now()

        with DB.session as session:

            def q(page=0, page_size=None):
                query = session.query(PlayLog).order_by(PlayLog.track_start.desc())
                if isinstance(from_time, datetime.datetime):
                    query = query.filter(
                        PlayLog.track_start >= from_time.isoformat(" ", "seconds")
                    )
                if isinstance(to_time, datetime.datetime):
                    query = query.filter(PlayLog.track_start <= to_time.isoformat(" ", "seconds"))
                if skip_synced:
                    query = query.filter(PlayLog.is_synced.is_(False))
                listen(query, "before_compile", apply_limit(page, page_size), retval=True)
                print("Paginate Query: " + str(query))
                return query

            def apply_limit(page, page_size):
                def wrapped(query):
                    if page_size:
                        query = query.limit(page_size)
                        if page:
                            query = query.offset(page * page_size)
                    return query

                return wrapped

            return q(page, page_size)

    @staticmethod
    def select_last_hours(n):
        """
        Select the tracks playing in the past (`n`) hours.
        """
        with DB.session as session:
            last_hours = datetime.datetime.today() - datetime.timedelta(hours=n)
            tracks = (
                session.query(PlayLog)
                .filter(PlayLog.track_start >= str(last_hours))
                .order_by(PlayLog.track_start.desc())
                .all()
            )
            return tracks

    @staticmethod
    def select_by_day(day):
        """
        Select the track-service items for a day.
        """
        day_plus_one = day + datetime.timedelta(days=1)
        with DB.session as session:
            tracks = (
                session.query(PlayLog)
                .filter(PlayLog.track_start >= str(day), PlayLog.track_start < str(day_plus_one))
                .order_by(PlayLog.track_start.desc())
                .all()
            )
            return tracks

    @staticmethod
    def select_by_range(from_day, to_day):
        """
        Select the track-service items for a day range.
        """
        with DB.session as session:
            tracks = (
                session.query(PlayLog)
                .filter(PlayLog.track_start >= str(from_day), PlayLog.track_start < str(to_day))
                .order_by(PlayLog.track_start.desc())
                .all()
            )
            return tracks

    def __str__(self):
        return "Track [track_start: %s, track_title: %s]" % (
            str(self.track_start),
            str(self.track_title),
        )


class PlayLogSchema(ma.SQLAlchemyAutoSchema):
    """
    Schema for playlog entries.
    """

    class Meta:
        """
        Inner meta class.
        """

        model = PlayLog
        sqla_session = DB.session

    SKIP_VALUES = set([None])

    @post_dump
    def remove_skip_values(self, data, many=False):
        """
        Remove all values from data which are present in "SKIP_VALUES".
        """
        return {key: value for key, value in data.items() if value not in self.SKIP_VALUES}


class ActivityLog(DB.Model):
    """
    Table holding a log of play-out source activity and their sync states.

    Only used in "SYNC" deployment mode.
    """

    __tablename__ = "activity_log"

    # Primary Key
    log_time = Column(DateTime, primary_key=True)

    # Columns
    source_number = Column(Integer)
    is_synced = Column(Boolean)

    def __init__(self, source_number):
        """
        Initialize an activity entry.
        """
        self.log_time = datetime.datetime.now()
        self.source_number = source_number
        self.is_synced = False

    @staticmethod
    def is_empty():
        """
        Check if the tables is empty.
        """
        with DB.session as session:
            return not session.query(ActivityLog).one_or_none()

    @staticmethod
    def get_active_source():
        """
        Retrieve the currently active source.
        """
        with DB.session as session:
            source = session.query(ActivityLog).order_by(ActivityLog.log_time.desc()).first()
            return source

    def save(self):
        """
        Save changes.
        """
        with DB.session as session:
            session.add(self)
            session.commit()


class HealthHistory(DB.Model):
    """
    Table holding an history of health information for sources.
    """

    __tablename__ = "health_history"

    # Primary Key
    log_time = Column(DateTime, primary_key=True)

    # Columns
    log_source = Column(Integer)  # The source the history entry relates to
    is_healthy = Column(Boolean)  # Indicates if source is "healthy enough" to be used for play-out
    is_synced = Column(Boolean)  # Only relevant for main nodes, in a multi-node setup
    health_info = Column(String(4096))  # Stringified JSON object or other, if needed

    def __init__(self, source_number, log_time, is_healthy, health_info):
        """
        Initialize an health entry.
        """
        self.log_time = log_time
        self.log_source = source_number
        self.is_healthy = is_healthy
        self.is_synced = False
        self.health_info = health_info

    @staticmethod
    def get_latest_entry(source_number):
        """
        Retrieve the most recent health history entry for the given source number.
        """
        with DB.session as session:
            return (
                session.query(HealthHistory)
                .filter(HealthHistory.log_source == source_number)
                .order_by(HealthHistory.log_time.desc())
                .first()
            )

    def save(self):
        """
        Save changes.
        """
        with DB.session as session:
            session.add(self)
            session.commit()


class HealthHistorySchema(ma.SQLAlchemyAutoSchema):
    """
    Schema for health history entries.
    """

    class Meta:
        """
        Inner meta class.
        """

        model = HealthHistory
        sqla_session = DB.session


class ClockInfo(DB.Model):
    """
    Table holding information for the current and next show to be displayed by the studio clock.

    Important: This table doesn't hold the history of information for efficiency.
    It only stores one record for each possible source.
    For example in a redundant deployment it holds two records, for engine1 and engine2.

    The stringified objects allow easy, future extension of the properties, without the need to
    change the database model.
    """

    __tablename__ = "clock_info"

    # Primary Key
    log_source = Column(
        Integer, primary_key=True
    )  # The source this entry was updated from ("1" for engine1, "2" for engine2)

    # Columns
    log_time = Column(DateTime)
    current_track = None  # Populated live from within `get_info(..)`
    planned_playlist = Column(
        String(4096)
    )  # Stringified "#/components/schemas/Playlist" OpenAPI JSON object
    current_timeslot = Column(
        String(2048)
    )  # Stringified "#/components/schemas/Timeslot" OpenAPI JSON object
    upcoming_timeslots = Column(
        String(2048)
    )  # Stringified "#/components/schemas/Timeslot" OpenAPI JSON object

    def __init__(self):
        """
        Initializes an clock info entry.
        """

    def set_info(self, source_number, planned_playlist, current_timeslot, upcoming_timeslots):
        """
        Set the values for a clock info entry.
        """
        self.log_time = datetime.datetime.now()
        self.log_source = source_number
        if planned_playlist:
            self.planned_playlist = json.dumps(planned_playlist.to_dict(), default=str)
        else:
            self.planned_playlist = None
        if current_timeslot:
            self.current_timeslot = json.dumps(current_timeslot.to_dict(), default=str)
        else:
            self.current_timeslot = None
        if upcoming_timeslots:
            upcoming_list = []
            for upcoming in upcoming_timeslots:
                upcoming_list.append(upcoming.to_dict())
            self.upcoming_timeslots = json.dumps(upcoming_list, default=str)
        else:
            self.upcoming_timeslots = None

    @staticmethod
    def get(source_number):
        """
        Retrieve the clock info for the given source number.
        """
        with DB.session as session:
            return session.query(ClockInfo).filter(ClockInfo.log_source == source_number).first()

    @staticmethod
    def get_info(source_number):
        """
        Retrieve the clock info for the given source number.
        """
        info = dict()
        with DB.session as session:
            data = session.query(ClockInfo).filter(ClockInfo.log_source == source_number).first()
            current_track = PlayLog.select_current()
            playlogs = None

            # Construct the clock `info` object
            if data:
                info["log_source"] = data.log_source
                info["log_time"] = data.log_time

                # Get the track currently playing
                if current_track:
                    info["current_track"] = current_track

                # Append the missing planned playlist items to the ones played
                if data.planned_playlist:
                    info["planned_playlist"] = json.loads(data.planned_playlist)
                else:
                    info["planned_playlist"] = {}

                # Get the current timeslot
                if data.current_timeslot:
                    info["current_timeslot"] = json.loads(data.current_timeslot)

                    # Get the most recent played track (because right now nothing might be playing)
                    most_recent_track = PlayLog.select_recent()

                    # Is the most recent track part of the current timeslot?
                    if most_recent_track.timeslot_id == info["current_timeslot"]["timeslot_id"]:
                        # Get the actual playlogs of the current timeslot, until now
                        playlogs = PlayLog.select_for_timeslot(most_recent_track.timeslot_id)
                        playlogs.sort(key=lambda track: track.track_start, reverse=False)
                        info["current_playlogs"] = playlogs
                        if info["current_playlogs"] is None:
                            info["current_playlogs"] = {}

                        # Invalid timeslots (e.g. in fallback scenarios) get a virtual start date
                        # of the first fallback track.
                        if info["current_timeslot"]["timeslot_id"] == -1:
                            if playlogs and playlogs[0]:
                                info["current_timeslot"]["timeslot_start"] = playlogs[
                                    0
                                ].track_start

                # Get the next timeslot
                if data.upcoming_timeslots:
                    info["upcoming_timeslots"] = json.loads(data.upcoming_timeslots)
                else:
                    info["upcoming_timeslots"] = {}

            return info

    def save(self):
        """
        Save changes.
        """
        with DB.session as session:
            session.add(self)
            session.commit()

    def update(self):
        """
        Update by merging remote changes.
        """
        with DB.session as session:
            session.merge(self)
            session.commit()


#
# Schemas for JSON responses
#


def camelcase(s):
    """
    Convert snake_case to camelCase.
    """
    parts = iter(s.split("_"))
    return next(parts) + "".join(i.title() for i in parts)


class CamelCaseSchema(Schema):
    """
    Schema uses camelCase for its external and snake_case for its external representation.
    """

    def on_bind_field(self, field_name, field_obj):
        """
        Executes camalcase func when field is bound.
        """
        field_obj.data_key = camelcase(field_obj.data_key or field_name)


class TrackSchema(CamelCaseSchema):
    """
    Schema for trackservice entries.
    """

    track_start = fields.Str(required=True)
    track_artist = fields.Str(required=True)
    track_album = fields.Str(required=True)
    track_title = fields.Str(required=True)
    track_duration = fields.Str(required=True)
    track_type = fields.Str(required=True)
    track_num = fields.Str(required=True)
    playlist_id = fields.Str(required=True)
    timeslot_id = fields.Str(required=True)
    show_id = fields.Str(required=True)
    show_name = fields.Str(required=True)


class TimeslotSchema(CamelCaseSchema):
    """
    Schema for timeslots.
    """

    show_name = fields.Str(required=True)
    show_id = fields.Str(required=True)
    timeslot_id = fields.Str(required=True)
    timeslot_start = fields.Str(required=True)
    timeslot_end = fields.Str(required=True)
    playlist_id = fields.Str(required=True)
    playlist_type = fields.Str(required=True)


class PlaylogSchema(CamelCaseSchema):
    """
    Schema for playlogs.
    """

    track_start = fields.Str(required=True)
    track_artist = fields.Str(required=True)
    track_album = fields.Str(required=True)
    track_title = fields.Str(required=True)
    track_duration = fields.Str(required=True)
    track_type = fields.Str(required=True)
    track_num = fields.Str(required=True)
    playlist_id = fields.Str(required=True)
    timeslot_id = fields.Str(required=True)
    show_id = fields.Str(required=True)
    show_name = fields.Str(required=True)
    log_source = fields.Str(required=True)
    custom_json = fields.Str(required=True)
    is_synced = fields.Str(required=True)


class PlaylistSchema(CamelCaseSchema):
    """
    Schema for playlogs.
    """

    playlist_id = fields.Str(required=True)
    entries = fields.List(fields.Nested(TrackSchema))


class ClockInfoSchema(CamelCaseSchema):
    """
    Schema for trackservice entries.
    """

    log_source = fields.Str(required=True)
    log_time = fields.Str(required=True)
    current_track = fields.Nested(TrackSchema)
    planned_playlist = fields.Nested(PlaylistSchema)
    current_playlogs = fields.List(fields.Nested(PlaylogSchema))
    current_timeslot = fields.Nested(TimeslotSchema)
    upcoming_timeslots = fields.Nested(TimeslotSchema)
