from flask import current_app

from aura_engine_api.rest import util
from aura_engine_api.rest.models.track import Track  # noqa: E501


def current_track():  # noqa: E501
    """Get current track

    Retrieves the currently playing track.  # noqa: E501


    :rtype: Track
    """
    service = current_app.config["SERVICE"]
    return service.current_track()


def list_tracks(from_date=None, to_date=None, page=None, limit=None):  # noqa: E501
    """List recent tracks in the play-log

    Lists the track-service entries for a given page.  # noqa: E501

    :param from_date: Get entries after this timestamp
    :type from_date: str
    :param to_date: Get entries before this timestamp
    :type to_date: str
    :param page: The number of items to skip before starting to collect the result set
    :type page: int
    :param limit: The numbers of items to return per page
    :type limit: int

    :rtype: List[Track]
    """
    service = current_app.config["SERVICE"]
    fd = None
    td = None

    if from_date is not None and to_date is not None:
        try:
            fd = util.deserialize_datetime(from_date)
            td = util.deserialize_datetime(to_date)
        except Exception:
            service.logger.warning(
                "Invalid 'from_date' (%s) or 'to_date' (%s) for 'list_tracks'"
                % (str(from_date), str(to_date))
            )

    return service.list_tracks(page, limit, fd, td)
