#
# Aura Engine API (https://gitlab.servus.at/aura/engine-api)
#
# Copyright (C) 2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
config.py.
"""

import logging
import os
import os.path
import re
import sys
from pathlib import Path

import confuse
import yaml

template = {
    "log": {
        "directory": str,
        "level": confuse.OneOf(["debug", "info", "warning", "error", "critical"]),
        "debug_flask": bool,
    },
    "db": {
        "type": confuse.OneOf(["postgresql", "mysql", "sqlite"]),
        "name": str,
        "user": str,
        "pwd": str,
        "host": str,
        "charset": str,
    },
    "api": {"port": int, "cors": str},
    "federation": {
        "enabled": bool,
        "default_source": int,
        "host_id": int,
        "sync_host": confuse.Optional(str),
        "main_host_1": confuse.Optional(str),
        "main_host_2": confuse.Optional(str),
        "sync": {
            "interval": int,
            "batch_size": int,
            "step_sleep": float,
            "api": {
                "get_playlog": str,
                "store_playlog": str,
                "store_healthlog": str,
                "store_clockinfo": str,
            },
        },
    },
}


class EnvVarLoader(yaml.SafeLoader):
    """Wrapper class for yaml.SafeLoader."""

    pass


class AuraConfig:
    """
    AuraConfig Class.

    Holds the Aura Configuration as in the file `engine-api.yaml`.
    """

    yaml_path = ""
    confuse_config = None
    config = None  # points to a validated config (hopefully later)
    logger = None

    def __init__(self, yaml_path):
        """
        Initializes the configuration, defaults to `/etc/aura/engine-api.yaml`.

        If this file doesn't exist it uses `./config/engine-api.yaml` from
        the project directory.

        Args:
            yaml_path(String):      The path to the configuration file `engine-api.yaml`
        """
        self.logger = logging.getLogger("engine-api")

        default_yaml_path = os.path.join(
            Path(__file__).parent.parent.parent.parent.absolute(),
            "config",
            "engine-api.yaml",
        )

        if yaml_path:
            config_file = Path(yaml_path)
            if not config_file.is_file():
                yaml_path = default_yaml_path
        else:
            yaml_path = default_yaml_path

        self.yaml_path = yaml_path

        if not os.path.isfile(self.yaml_path):
            self.logger.critical(self.yaml_path + " not found")
            sys.exit(1)

        self.logger.info(f"Using configuration at '{yaml_path}'")

        path_matcher = re.compile(r"\$\{([^}^{]+)\}")

        def path_constructor(loader, node):
            return os.path.expandvars(node.value)

        EnvVarLoader.add_implicit_resolver("!path", path_matcher, None)
        EnvVarLoader.add_constructor("!path", path_constructor)

        self.confuse_config = confuse.Configuration("engine-api", loader=EnvVarLoader)
        self.confuse_config.set_file(yaml_path)
        self.confuse_config.set_env(prefix="AURA_ENGINE_API_", sep="_")

        # custom overrides and defaults
        self.confuse_config["install_dir"].set(os.path.realpath(__file__ + ".."))
        self.confuse_config["api_prefix"].set("/api/v1")

        # fetch config values using template
        try:
            self.config = self.confuse_config.get(template)
        except (confuse.ConfigValueError, confuse.NotFoundError) as e:
            self.logger.error(e)

    def set(self, key, value):
        """
        Setter for some specific config property.

        Args:
            key (String):   key
            default (*):    value
        """
        try:
            parsed_value = int(value)
        except ValueError:
            parsed_value = str(value)
        self.__dict__[key] = parsed_value

    def get_database_uri(self):
        """
        Retrieves the database connection string.
        """
        db_cfg = self.config.db
        db_name = db_cfg.name
        db_type = db_cfg.type

        if db_type in {"mysql", "postgresql"}:
            db_user = db_cfg.user
            db_pass = db_cfg.pwd
            db_host = db_cfg.host
            db_charset = db_cfg.charset
            if db_type == "mysql":
                return f"mysql://{db_user}:{db_pass}@{db_host}/{db_name}?charset={db_charset}"
            else:
                return (
                    f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}/{db_name}"
                    f"?client_encoding={db_charset}"
                )
        elif db_type == "sqlite":
            # "db_name" is expected to be either a relative or an absolute path to the sqlite file
            return f"sqlite:///{db_name}.db"
        else:
            return f"Error: invalid database type '{db_type}'"
