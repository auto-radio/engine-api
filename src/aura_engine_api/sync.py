#
# Aura Engine API (https://gitlab.servus.at/aura/engine-api)
#
# Copyright (C) 2020 - The Aura Engine Team.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""
sync.py.
"""

import threading
import time
from contextlib import contextmanager

import requests
from sqlalchemy.exc import IntegrityError, InvalidRequestError
from sqlalchemy.orm import scoped_session, sessionmaker

from aura_engine_api.models import ActivityLog, PlayLog, camelcase
from aura_engine_api.rest.models.play_log import PlayLog as PlayLogAPI


class SyncJob(threading.Thread):
    """
    Job periodically checking for data at one of the main nodes to be synchronized.

    This job should be running on a "Sync Node" only.
    """

    config = None
    logger = None
    app = None
    exit_event = None
    sync_interval = None
    sync_batch_size = None

    session_factory = None
    Session = None

    def __init__(self, config, logger, app, engine):
        """
        Initialize Job.
        """
        self.config = config
        self.logger = logger
        self.app = app
        self.session_factory = sessionmaker(autoflush=False, autocommit=False, bind=engine)
        threading.Thread.__init__(self)
        self.exit_event = threading.Event()
        self.sync_interval = self.config.federation.sync.interval
        self.sync_batch_size = self.config.federation.sync.batch_size
        self.logger.info(
            "Initialized Sync Job - Synchronizing API Nodes every %s seconds and with a max"
            " batch-size of %s.",
            self.sync_interval,
            self.sync_batch_size,
        )

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""
        session = scoped_session(self.session_factory)
        yield session
        session.close()

    def run(self):
        """
        Starts the Job.
        """
        self.synchronize()
        while not self.exit_event.wait(self.sync_interval):
            try:
                self.synchronize()
                self.logger.info("Sync cycle done.\n\n")
            except Exception:
                self.logger.info(
                    "Error while syncing entries. Maybe there's some connectivity issue."
                    " Aborting cycle ..."
                )

    def synchronize(self):
        """
        Performs the synchronization with the main nodes.
        """
        with self.session_scope() as session:
            entries = None
            synced_entries = 0
            unsynced_sources = self.get_unsynced_history(session)
            self.logger.info(
                "Synchronization of API Nodes: There are %s sources open to be synced."
                % len(unsynced_sources)
            )

            for i in range(len(unsynced_sources)):
                source = unsynced_sources[i]
                self.logger.info(
                    "Syncing source %s which is unsynced since %s"
                    % (source.source_number, source.log_time)
                )

                # Store the next source to build a datetime range
                next_source = None
                if i + 1 < len(unsynced_sources):
                    next_source = unsynced_sources[i + 1]
                else:
                    next_source = self.create_next_source_log(source, session)

                # Get unsynced entries
                page = 0
                entries = self.get_unsynced_entries(source, next_source, page)

                while entries and len(entries) > 0:
                    if not entries:
                        self.logger.info("Retrieved no entries to be synced")
                    else:
                        self.logger.info("Retrieved %s playlogs to be synced" % len(entries))

                    # Store unsynced entries locally
                    for entry in entries:
                        try:
                            session.begin_nested()
                            # FIXME: expecting no need for manual camelcase conversion
                            entry_cc = dict()
                            for k, v in entry.items():
                                entry_cc[camelcase(k)] = v
                            #
                            entry = PlayLogAPI.from_dict(entry_cc)
                            playlog = PlayLog(entry)
                            playlog.is_synced = True
                            self.db_save(playlog, session)
                            self.logger.info(
                                "Stored synced playlog for '%s'" % playlog.track_start
                            )
                            synced_entries += 1
                        except (IntegrityError, InvalidRequestError):
                            session.rollback()
                            self.logger.info(
                                "Playlog for '%s' already exists in local database. Skipping..."
                                % playlog.track_start
                            )

                        # Sleep a little to keep the effective load down low
                        time.sleep(self.config.federation.sync.step_sleep)

                    # Get some more unsynced entries, if available
                    page += 1
                    entries = self.get_unsynced_entries(source, next_source, page)

                # Update state of the source activity log to `synced=true`
                try:
                    source.is_synced = True
                    self.db_save(source, session)
                    self.logger.info(
                        "Sync for source %s:%s finalized!"
                        % (source.source_number, source.log_time)
                    )
                except Exception as e:
                    self.logger.error(
                        "Cannot finalize sync state for source=%s:%s - Reason: %s"
                        % (source.source_number, source.log_time, str(e))
                    )

                # For now, let it be ... if there's more to sync let's do it in the next cycle
                self.logger.info("... successfully synchronized %s playlogs!" % synced_entries)

    def create_next_source_log(self, current_source, session):
        """
        Create and store the next source in the ActivityLog.

        It's actually the same, as the current, but acts as a references for the current sync
        and as a marker for the entrypoint of the next sync -> to avoid unnecessary sync cycles.
        """
        next_source = ActivityLog(current_source.source_number)
        session.add(next_source)
        session.commit()
        return next_source

    def db_save(self, model, session):
        """
        Store some object to the database using the local, scoped session.
        """
        session.add(model)
        session.commit()

    def get_unsynced_history(self, session):
        """
        Retrieves all sources with un-synced states.
        """
        unsynced = (
            session.query(ActivityLog)
            .filter(ActivityLog.is_synced.is_(False))
            .order_by(ActivityLog.log_time.asc())
        )
        result = unsynced.all()
        return result

    def get_unsynced_entries(self, source, next_source, page):
        """
        Retrieve unsynced entries from main node.
        """
        response = None
        url = None
        to_time = None
        if next_source:
            to_time = next_source.log_time

        try:
            params = {
                "page": page,
                "limit": self.sync_batch_size,
                "skip_synced": "true",
                "from_date": source.log_time,
                "to_date": to_time,
            }
            url = self.get_url(source.source_number)
            response = requests.get(url, params=params)
            if response.status_code == 200:
                self.logger.info("Response from '%s' OK (200)" % url)
                return response.json()
            else:
                msg = "Invalid status code while getting unsynced entries from remote API: " + str(
                    response.status_code
                )
                self.logger.warn(msg)
                raise Exception(msg)
        except Exception as e:
            self.logger.warn(
                "Error while getting unsynced entries from remote API '%s'!\n%s" % (url, str(e))
            )
            raise e

    def get_url(self, source_number):
        """
        Builds an URL for the remote API.
        """
        url = self.config.federation["main_host_" + str(source_number)]
        url += self.config.federation.sync.api.get_playlog
        return url

    def exit(self):
        """
        Called when the application shuts down.
        """
        self.exit_event.set()
