# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- `make api`: generate, merge and clean files according to api specs in `schemas/openapi-engine.yaml` (#37)

### Changed

- Use yaml instead of ini files for configuration (#34)

### Deprecated

- ...

### Removed

- Remove endpoint `/playlog/report/{year_month}` (#48)

### Fixed

- ...

### Security

- ...

## [1.0.0-alpha2] - 2023-06-19

### Added

- Test coverage (`make coverage` #40)
- Badge displaying coverage result (#40)

### Changed

- Make properties in API schemas in CamelCase notation (aura#141)
- Avoid deprecation warning by replacing JSON encoder (#35)
- Change HTTP status codes (204 No Content) for successful but empty POST/PUT responses, adapt tests (#5)

### Fixed

- Fix an issue where the configuration file is overwritten in the `make init.dev` target
- Fix an issue where the Docker Compose healthcheck failed (#39)
- Fix broken endpoint `/trackservice` (aura#185)

## [1.0.0-alpha1] - 2023-02-28

Initial release.

