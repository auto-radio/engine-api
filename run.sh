#!/bin/bash
# Default mode
mode="dev"
docker="false"

#
# Run Script for AURA Engine API
#
# Call with one of these parameters:
#
# - dev
# - prod
# - test
# - recreate-database

# - docker:recreate-database
# - docker:build
# - docker:push
# - docker:dev
#

if [[ $* =~ ^(dev|api-test-0|api-test-1|api-test-2|prod|test)$ ]]; then
	mode=$1
fi

if [[ "$1" == *"docker:"* ]]; then
	docker="true"
	mode=${1#*:}
fi


echo "[ Run mode=$mode ]"
echo "[ Docker=$docker ]"

# Check for the correct Python version (3.8+)
PYTHON_EXEC="python3"
PYTHON_BASE_DIR="src"
echo "[ Using $("$PYTHON_EXEC" -V) ]"


# +++ DEFAULT COMMANDS +++ #

if [[ $docker == "false" ]]; then
	export PYTHONPATH="$PYTHON_BASE_DIR"

	### Runs the API Server (Development) ###

	if [[ $mode == "dev" ]]; then
		source python/bin/activate
		echo "Running Engine API in Python Environment ($("$PYTHON_EXEC" -V))"
		echo "Starting API Server"
		"$PYTHON_EXEC" -m aura_engine_api.app
	fi

	### Runs the API Server (Test) ###

	if [[ $mode == "api-test-0" ]]; then
		echo "Starting API Server 0"
		"$PYTHON_EXEC" -m aura_engine_api.app config=tests/config/engine-0-api.yaml
	fi
	if [[ $mode == "api-test-1" ]]; then
		echo "Starting API Server 1"
		"$PYTHON_EXEC" -m aura_engine_api.app config=tests/config/engine-1-api.yaml
	fi
	if [[ $mode == "api-test-2" ]]; then
		echo "Starting API Server 2"
		"$PYTHON_EXEC" -m aura_engine_api.app config=tests/config/engine-2-api.yaml
	fi

	### Runs the API Server using Gunicorn without a system daemon (Production) ###

	if [[ $mode == "prod" ]]; then
		echo "Starting API Server"
		gunicorn -c config/gunicorn.conf.py aura_engine_api.app:app
	fi

	if [[ $mode == "test" ]]; then
		echo "Testing API Server"
		tox
	fi

	### CAUTION: This deletes everything in your database ###

	if [[ $mode == "recreate-database" ]]; then
		"$PYTHON_EXEC" -m aura_engine_api.app --recreate-database
	fi

fi


# +++ DOCKER COMMANDS +++ #

if [[ $docker == "true" ]]; then
	BASE_D=$(realpath "${BASH_SOURCE%/*}/")

	### Runs Engine API using Gunicorn ###

	if [[ $mode == "dev" ]]; then
		exec sudo docker run \
			--network="host" \
			--name aura-engine-api \
			--rm \
			-u $UID:$GID \
			-v "$BASE_D":/srv \
			-v "$BASE_D/config/docker":/srv/config \
			--tmpfs /var/log/aura/ \
			-e TZ=Europe/Vienna \
			autoradio/engine-api
	fi

	### Create Docker Image from local project ###

	if [[ $mode == "build" ]]; then
		exec sudo docker build -t autoradio/engine-api .
	fi

	### Pushes the latest Docker Image to Docker Hub ###

	if [[ $mode == "push" ]]; then
		exec sudo docker push autoradio/engine-api
	fi
fi
