# Engine API Server

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md) [![Latest Release](https://gitlab.servus.at/aura/engine-api/-/badges/release.svg)](https://gitlab.servus.at/aura/engine-api/-/releases) [![pipeline status](https://gitlab.servus.at/aura/engine-api/badges/main/pipeline.svg)](https://gitlab.servus.at/aura/engine-api/-/commits/main) [![coverage](https://gitlab.servus.at/aura/engine-api/badges/main/coverage.svg)](https://gitlab.servus.at/aura/engine-api/-/commits/main)


<img src="https://gitlab.servus.at/aura/aura/-/raw/main/assets/images/aura-engine.png" width="120" align="right" />

Engine API stores and provides playlog and studio clock information for the playout engine.

Its features include:

- **Playlogs**: A history of all audio played by the Engine.
- **Track Service**: Same as playlogs, but stripped-down information for public use.
- **Studio Clock**: Information on the current and next shows to be used in a _Studio Clock_ application.
- **Health Information**: The history of health status records.
- **Data Synchronization**: In high-availability deployment scenarios, data is aggregated from various Engine API instances. This can be extended to other audio sources, like physical silence detectors or other studio equipment.

This documentation is primarily meant for developers. For using the _AURA Community Radio Suite_
visit [aura.radio](https://aura.radio).

## Prerequisites

- [Python 3.9+](https://www.python.org/)
- [`pip`](https://pip.pypa.io/en/stable/)
- [`git`](https://git-scm.com/)
- [PostgreSQL 13+](https://www.postgresql.org/)

## Preparation

### Initialize environment

Install dependencies and prepare config file

```shell
make init.app
```

This also creates a default configuration file at `config/engine-api.yaml`.

For development install with

```shell
make init.dev
```

Note, if some configuration exists under `/etc/aura/engine-api.yaml` the configuration by default is drawn from there. This overrides any configuration located in the local configuration file.

### Initialize database

Engine requires a PostgreSQL database to cache any programme info locally.

```bash
sudo -u postgres psql -f contrib/postgresql-create-database.sql
```

This creates a database and tables with default password `1234`.

## Configuration

Edit the configuration file at `config/engine-api.yaml`. Verify or change the database password.

```ini
db_pass="1234"
```

## Running Engine

To start the Engine execute

```shell
make run
```

## Docker

For production deployments follow the Docker Compose installation instruction for _AURA Playout_ at [docs.aura.radio](https://docs.aura.radio/).

The following instructions are meant for development.

### Build with Docker

Build your own, local Docker image

```shell
make docker.build
```

### Run with Docker

Run the locally build image

```shell
make docker.run
```

### Release to DockerHub

Releasing a new version to DockerHub

```shell
make docker.push
```

Usually this is not required, as this is automatically performed by the CI/CD pipeline.

## Development

This project is based on a [Flask](https://flask.palletsprojects.com/) server using an [_API First_](https://swagger.io/resources/articles/adopting-an-api-first-approach/) approach. The API is specified using [Open API 3](https://swagger.io/specification/). The resulting API implementation is utilizing the popular [Connexion](https://github.com/zalando/connexion) library on top of Flask.

### Using the API as a developer

You can find details on the available API endpoints at [api.aura.radio](https://api.aura.radio/).

Adding some entry to the playlog:

```bash
curl -d '{ "track_start": "2020-06-27 19:14:00", "track_artist": "Mazzie Star", "track_title": "Fade Into You", "log_source": 1 }' -H "Content-Type: application/json" -X POST http://localhost:8008/api/v1/playlog
```

This newly added entry can be queried using your browser in one of the following ways:

```bash
# Get the latest entry
http://localhost:8008/api/v1/trackservice/current
# Get a set of the most recent entries
http://localhost:8008/api/v1/trackservice/
# Filter some specific page (the three most recent entries)
http://localhost:8008/api/v1/trackservice?page=0&limit=3
```

The local OpenAPI UI can be found at [/api/v1/ui/](http://localhost:8008/api/v1/ui/) and your OpenAPI definition lives at [/api/v1/openapi.json](http://localhost:8008/api/v1/openapi.json):

### Extending the API as a developer

The workflow for extending the API follows the **API First** approach. This means you have to edit the API at `./schemas/openapi-engine.yaml`, execute `make api` and review the changes using git.

Note that existing implementations in controller classes and tests will be overwritten - make sure not to commit these changes.


## Read more

- [Advanced configuration of Engine API](docs/advanced-config.md)
- [Engine Overview](https://gitlab.servus.at/aura/engine)
- [docs.aura.radio](https://docs.aura.radio)
- [aura.radio](https://aura.radio)
