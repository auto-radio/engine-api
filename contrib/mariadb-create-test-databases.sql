CREATE DATABASE aura_engine_api_0 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE aura_engine_api_1 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE DATABASE aura_engine_api_2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE USER 'aura_engine_api'@'localhost' IDENTIFIED BY '1234';

GRANT ALL PRIVILEGES ON aura_engine_api_0.* TO 'aura_engine_api'@'localhost';
GRANT ALL PRIVILEGES ON aura_engine_api_1.* TO 'aura_engine_api'@'localhost';
GRANT ALL PRIVILEGES ON aura_engine_api_2.* TO 'aura_engine_api'@'localhost';

FLUSH PRIVILEGES;