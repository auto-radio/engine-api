\c postgres
create database aura_engine_api_0;
create database aura_engine_api_1;
create database aura_engine_api_2;
create user aura_engine_api with encrypted password '1234';
grant all privileges on database aura_engine_api_0 to aura_engine_api;
grant all privileges on database aura_engine_api_1 to aura_engine_api;
grant all privileges on database aura_engine_api_2 to aura_engine_api;
