CREATE DATABASE aura_engine_api CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'aura_engine_api'@'localhost' IDENTIFIED BY '1234';
GRANT ALL PRIVILEGES ON aura_engine_api.* TO 'aura_engine_api'@'localhost';
FLUSH PRIVILEGES;