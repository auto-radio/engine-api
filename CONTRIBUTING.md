# Contributing

When contributing to this repository, please first read about [contributing to AURA](https://docs.aura.radio/en/latest/contribute/contributions.html). Then discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Please note we have a [code of conduct](https://docs.aura.radio/en/latest/contribute/code_of_conduct.html), please follow it in all your interactions with the project.
