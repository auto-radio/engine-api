
FROM python:3.10-slim as base
LABEL maintainer="David Trattnig <david@subsquare.at>"

ENV AURA_UID=2872
ENV AURA_GID=2872

# Configure Poetry
ENV POETRY_VERSION=1.3.2
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VENV=/opt/poetry-venv
ENV POETRY_CACHE_DIR=/opt/.cache

# System Dependencies
RUN apt-get update && apt-get -y install \
    apt-utils \
    build-essential \
    pip \
    ncat

# Setup Poetry
RUN python3 -m venv $POETRY_VENV \
    && $POETRY_VENV/bin/pip install -U pip setuptools \
    && $POETRY_VENV/bin/pip install poetry==${POETRY_VERSION}
ENV PATH="${PATH}:${POETRY_VENV}/bin"

# Base Structure
RUN mkdir -p /srv/src/aura_engine_api /srv/config /srv/logs /etc/aura
COPY pyproject.toml poetry.lock README.md Makefile /srv/

WORKDIR /srv

# Init Application
COPY ./src/aura_engine_api /srv/src/aura_engine_api
COPY ./schemas /srv/schemas
COPY ./config/sample/gunicorn/sample-docker.gunicorn.conf.py /srv/config/gunicorn.conf.py
COPY ./config/sample.engine-api.docker.yaml /srv/config/engine-api.yaml
RUN poetry install --no-interaction --no-ansi

# Update Permissions
RUN groupadd --gid ${AURA_GID} aura && \
    useradd --gid ${AURA_GID} --no-user-group --uid ${AURA_UID} --home-dir /srv --no-create-home aura && \
    chown -R ${AURA_UID}:${AURA_GID} /srv /etc/aura


USER aura

# Start Service
EXPOSE 8008
ENTRYPOINT ["make"]
CMD ["run.gunicorn"]
