[project]
name = "aura-engine-api"
requires-python = "^3.10"

[tool.black]
line-length = 99
target-version = ["py310"]
# TODO: Use extend-exclude as soon as Debian Bookworm is released.
exclude = '''
    ^/src/aura_engine_api/rest/
    | ^/config/
    | ^/python/
    | ^/.tox/
'''

[tool.isort]
py_version = 310
profile = "black"
# TODO: switch to "extend_skip", after Debian Bookworm is released
skip = [
    "config",
    "node_modules",
    "python",
    "src/aura_engine_api/rest/",
    ".git",
    ".tox",
]

[tool.poetry]
name = "aura-engine-api"
version = "1.0.0-alpha2"
description = "AURA Engine API providing playlog and clock information."
license = "AGPL-3.0-or-later"
authors = ["David Trattnig <david@subsquare.at>"]
maintainers = ["David Trattnig <david@subsquare.at>"]
readme = "README.md"
homepage = "https://aura.radio/"
repository = "https://gitlab.servus.at/aura/engine-api"
documentation = "https://docs.aura.radio/"
keywords = ["radio", "scheduling", "audio", "playout"]
classifiers = [
    "Topic :: Multimedia :: Sound/Audio",
    "Topic :: Multimedia :: Sound/Audio :: Mixers",
    "Topic :: Multimedia :: Sound/Audio :: Players",
]
packages = [{ include = "aura_engine_api", from = "src" }]

[tool.poetry.dependencies]
python = "^3.10"
requests = "^2.24"
connexion = "^2.8.0"
python-dateutil = "^2.6.0"
typing = "^3.5.2.2"
gunicorn = "^20.0.40"
SQLAlchemy = "^1.3.17"
Flask-SQLAlchemy = "^2.4.3"
flask-marshmallow = "^0.13.0"
marshmallow-sqlalchemy = "^0.23.1"
swagger-ui-bundle = "^0.0.2"
click = "^8.0.0"
Flask-Testing = "^0.8.1"
psycopg2-binary = "^2.9.3"
pre-commit = "^3.3.1"
coverage = "^7.2.7"
openapi-generator-cli = "^4.3.1"
confuse = "^2.0.1"

[tool.poetry.group.dev.dependencies]
codespell = "^2.2.1"
black = "^22.8.0"
flake8 = "^5.0.4"
# Add target after #28 is done (@see https://gitlab.servus.at/aura/engine-api/-/issues/28)
#flake8-docstrings = "^1.6.0"
validators = "^0.20.0"
isort = "^5.10.1"
flake8-docstrings = "^1.7.0"

[build-system]
requires = ["poetry>=1.3"]
build-backend = "poetry.masonry.api"
